<?php

function hitung ($string_data)
{
    if (strstr($string_data, "*") != false){
        $arr_number = explode("*",$string_data);
        $hasil_perhitungan = $arr_number[0] * $arr_number[1];
        return strval($hasil_perhitungan);
      }
      if (strstr($string_data, "+") != false){
        $arr_number = explode("+",$string_data);
        $hasil_perhitungan = $arr_number[0] + $arr_number[1];
        return strval($hasil_perhitungan);
      }
      if (strstr($string_data, ":") != false){
        $arr_number = explode(":",$string_data);
        $hasil_perhitungan = $arr_number[0] / $arr_number[1];
        return strval($hasil_perhitungan);
      }
      if (strstr($string_data, "%") != false){
        $arr_number = explode("%",$string_data);
        $hasil_perhitungan = $arr_number[0] % $arr_number[1];
        return strval($hasil_perhitungan);
      }
      if (strstr($string_data, "-") != false){
        $arr_number = explode("-",$string_data);
        $hasil_perhitungan = $arr_number[0] - $arr_number[1];
        return strval($hasil_perhitungan);
      }
}

//Test cases

echo hitung ("102*2");
echo "<br>";
echo hitung("2+3");
echo "<br>";

echo hitung("100:25");
echo "<br>";

echo hitung("10%2");
echo "<br>";

echo hitung("99-2");
echo "<br>";

?>