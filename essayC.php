CREATE TABLE customers (
    id int  AUTO_INCREMENT PRIMARY KEY,
    Name varchar(255),
    email varchar(255),
    password varchar(255),
);
CREATE TABLE orders (
    id int AUTO_INCREMENT PRIMARY KEY,
    amount varchar(255),
    customer_id FOREIGN KEY (customer_id) REFERENCES customers(customer_id),

);
